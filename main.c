#include <stdio.h>
#include "util.h"
#include "table.h"
#include "data.h"
#include "data_iterator.h"



#define DEBUG_MODE
#ifdef DEBUG_MODE
#define DEBUG_PRINT(...) do{ fprintf( stderr, __VA_ARGS__ ); } while( false )
#else
#define DEBUG_PRINT(...) do{ } while ( false )
#endif



void read_where_test(FILE* file) {

    DEBUG_PRINT("----| read_where test start |----\n");

    database* db = initialize_database(file, false);

    table* tb = open_table(db, "my_table");

    data_iterator* iter1 = initialize_iterator(db, tb);

    const char* str = "mystr";

    int32_t a;
    char* b = malloc(255 * sizeof(char));
    bool c;

    while (seek_next_where(iter1, "description", STRING, (void*) &str)) {

        get_integer(iter1, "id", &a);

        get_string(iter1, "description", &b);

        get_bool(iter1, "broken", &c);

        DEBUG_PRINT("%d %s %d\n", a, b, c);
    }

    close_database(db);
    DEBUG_PRINT("----| read_where test end |----\n");

}


void test_join(FILE* file) {
    DEBUG_PRINT("---- join test start ----\n");
    database* db = initialize_database(file, false);

    table* tb1 = open_table(db, "my_table");
    table* tb2 = open_table(db, "your_table");

    print_join_table(db, tb1, tb2, "id", "amount", INTEGER);

}

void init_schema(database* db) {
    table* tb = create_new_table("my_table", 3);
    initialize_fixed_size_column(tb, 0, "id", INTEGER);
    initialize_var_size_column(tb, 1, "description", STRING, 40);
    initialize_fixed_size_column(tb, 2, "broken", BOOLEAN);
    table_apply(db, tb);

    table* tb2 = create_new_table("your_table", 2);
    initialize_fixed_size_column(tb2, 0, "amount", INTEGER);
    initialize_var_size_column(tb2, 1, "title", STRING, 100);
    table_apply(db, tb2);

    close_table(tb);
    close_table(tb2);
}

time_t test_write(database* db, int rowsAmount) {
    DEBUG_PRINT("----| write test start |----\n");
    table *tb, *tb2;
    tb = open_table(db, "my_table");
    tb2 = open_table(db, "your_table");

    struct timespec start = get_current_time();

    for (int i = 0; i < rowsAmount; i++) {

        data *dt1 = initialize_data(tb);
        initialize_integer_data(dt1, i, "id");
        initialize_string_data(dt1, "mystr", "description");
        initialize_bool_data(dt1, i % 2, "broken");
        insert_data(dt1, db);

        data *dt2 = initialize_data(tb);
        initialize_integer_data(dt2, 16, "id");
        initialize_string_data(dt2, i % 2 ? "candy" : "CANDY", "description");
        initialize_bool_data(dt2, (i + 1) % 2, "broken");
        insert_data(dt2, db);

        data *dt3 = initialize_data(tb2);
        initialize_integer_data(dt3, i, "amount");
        initialize_string_data(dt3, "coolol", "title");
        insert_data(dt3, db);

    }
    struct timespec finish = get_current_time();
    DEBUG_PRINT("----| write test end |----\n");
    close_table(tb);
    close_table(tb2);
    return get_time_difference_microseconds(start, finish);
}

time_t test_read_all(database* db) {
    DEBUG_PRINT("----| read test start |----\n");

    table* tb = open_table(db, "my_table");

    data_iterator* iter1 = initialize_iterator(db, tb);
    int32_t a;
    char* b = malloc(255 * sizeof(char));
    bool c;

    struct timespec start = get_current_time();
    int count = 0;
    while (seek_next(iter1)) {
        get_integer(iter1, "id", &a);
        get_string(iter1, "description", &b);
        get_bool(iter1, "broken", &c);
        count++;
        DEBUG_PRINT("%d %s %d\n", a, b, c);
    }
    struct timespec finish = get_current_time();

    DEBUG_PRINT("----| read test finished |----\n");

    close_table(tb);
    free(b);
    free(iter1);
    return get_time_difference_microseconds(start, finish);
}

time_t test_update(database* db, char* whereColdescription, columnType whereColType, void* whereVal,  char* updateColdescription, columnType updateColType, void* updateTo) {

    DEBUG_PRINT("----| update test start |----\n");

    table* tb = open_table(db, "my_table");

    struct timespec start = get_current_time();
    uint16_t updatedRows = update_where(db, tb, whereColdescription, whereColType, whereVal, updateColdescription, updateColType, updateTo);
    struct timespec finish = get_current_time();
    close_table(tb);
    DEBUG_PRINT("----| update test end. rows updated: %d |----\n", updatedRows);
    return get_time_difference_microseconds(start, finish);
}

time_t test_delete(database* db, char* wheredescription, columnType whereType, void* value) {
    DEBUG_PRINT("----| delete test start |----\n");

    table* tb = open_table(db, "my_table");
    const char* str = "mystr";
    int ts = 5;

    struct timespec start = get_current_time();
    uint16_t deletedRows = delete_where(db, tb, wheredescription, whereType, value);
    struct timespec finish = get_current_time();
    DEBUG_PRINT("----| delete test end. rows deleted: %d |----\n", deletedRows);
    close_table(tb);
    return get_time_difference_microseconds(start, finish);
}

void print_time(long long time[], int length) {
    printf("Time: [");
    for (int i = 0; i < length; i++) {
        printf("%lld ", time[i]);
    }
    printf("]\n");
}

int main() {
    char* filedescription = "database.db";
    FILE* file = fopen(filedescription, "wb+");
    if (!file) {
        DEBUG_PRINT("Unable to open file.");
        exit(EXIT_FAILURE);
    }
    database* db = initialize_database(file, true);
    init_schema(db);
    const int writeChecksAmount = 25;
    const int accessChecksAmount = 10;

    test_write(db, 100);
    long long writeTimes[writeChecksAmount];
    for (int i = 0; i < writeChecksAmount; i++) {
        writeTimes[i] = test_write(db, 2);
        test_write(db, 100);
    }
    printf("___WRITE TIME___: \n");
    print_time(writeTimes, writeChecksAmount);


    long long readTimes[accessChecksAmount];
    long long updateTimes[accessChecksAmount];
    long long deleteTimes[accessChecksAmount];

    for (int i = 0; i < accessChecksAmount; ++i) {
        test_write(db, 10000);
        readTimes[i] = test_read_all(db);

        char* whereVal = "CANDY";
        bool updateTo = true;
        updateTimes[i] = test_update(db, "broken", STRING, &whereVal, "broken", BOOLEAN, &updateTo);

        char* deleteValue = "MYVA";
        deleteTimes[i] = test_delete(db, "description", STRING, &deleteValue);
    }


    printf("\t___READ TIME___: \n");
    print_time(readTimes, accessChecksAmount);

    printf("\t___UPDATE TIME___: \n");
    print_time(updateTimes, accessChecksAmount);

    printf("\t___DELETE TIME___: \n");
    print_time(deleteTimes, accessChecksAmount);

    test_join(file);
    close_database(db);
    return 0;
}
