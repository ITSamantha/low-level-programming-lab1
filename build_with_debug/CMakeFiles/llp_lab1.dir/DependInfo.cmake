
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/samantha/Desktop/llp1_my/low-level-programming-lab1-master/data.c" "CMakeFiles/llp_lab1.dir/data.c.o" "gcc" "CMakeFiles/llp_lab1.dir/data.c.o.d"
  "/home/samantha/Desktop/llp1_my/low-level-programming-lab1-master/data_iterator.c" "CMakeFiles/llp_lab1.dir/data_iterator.c.o" "gcc" "CMakeFiles/llp_lab1.dir/data_iterator.c.o.d"
  "/home/samantha/Desktop/llp1_my/low-level-programming-lab1-master/database.c" "CMakeFiles/llp_lab1.dir/database.c.o" "gcc" "CMakeFiles/llp_lab1.dir/database.c.o.d"
  "/home/samantha/Desktop/llp1_my/low-level-programming-lab1-master/main.c" "CMakeFiles/llp_lab1.dir/main.c.o" "gcc" "CMakeFiles/llp_lab1.dir/main.c.o.d"
  "/home/samantha/Desktop/llp1_my/low-level-programming-lab1-master/table.c" "CMakeFiles/llp_lab1.dir/table.c.o" "gcc" "CMakeFiles/llp_lab1.dir/table.c.o.d"
  "/home/samantha/Desktop/llp1_my/low-level-programming-lab1-master/util.c" "CMakeFiles/llp_lab1.dir/util.c.o" "gcc" "CMakeFiles/llp_lab1.dir/util.c.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
