#include "data.h"


// Initialize data for database
data* initialize_data(table* tb) {
    data* dt = malloc(sizeof(data));
    dataHeader* dHeader = malloc(sizeof (dataHeader));
    dHeader->is_valid = true;
    dHeader->nextInvalidRowPage = -1; 
    dHeader->nextInvalidRowOrdinal = -1;
    dt->table = tb;
    dt->bytes = malloc(tb->header->oneRowSize);
    memcpy(dt->bytes, dHeader, sizeof(dataHeader));
    dt->pointer = sizeof(dataHeader);
    dt->dataHeader = dHeader;
    return dt;
}

//Initialize bool data
void initialize_bool_data(data* dt, bool val, char* columnName) {
    columnHeader* header = get_column_header_by_name(dt->table, columnName);
    char* pointer = (char*) dt->bytes + dt->pointer;
    *((bool*) (pointer)) = val;
    dt->pointer += header->size;
}

//Initialize integer data
void initialize_integer_data(data* dt, int32_t val, char* columnName) {
    columnHeader* header = get_column_header_by_name(dt->table, columnName);
    char* pointer = (char*) dt->bytes + dt->pointer;
    *((int32_t*) (pointer)) = val;
    dt->pointer += header->size;
}

//Initialize float data
void initialize_float_data(data* dt, double val, char* columnName) {
    columnHeader* header = get_column_header_by_name(dt->table, columnName);
    char* pointer = (char*) dt->bytes + dt->pointer;
    *((double*) (pointer)) = val;
    dt->pointer += header->size;
}

//Initialize string data
void initialize_string_data(data* dt, char* val, char* columnName) {
    columnHeader* header = get_column_header_by_name(dt->table, columnName);
    char* pointer = (char*) dt->bytes + dt->pointer;
    strcpy((char *) (pointer), val);
    dt->pointer += header->size;
}

// Initialize data for database. Depends on type.
void initialize_custom_data(data* dt, columnType type, void* val, char* columnName) {
    switch (type) {
        case INTEGER:
            initialize_integer_data(dt, *((int32_t*) val), columnName);
            break;
        case STRING:
            initialize_string_data(dt, *((char**) val), columnName);
            break;
        case BOOLEAN:
            initialize_bool_data(dt, *((bool*) val), columnName);
            break;
        case FLOAT:
            initialize_float_data(dt, *((double*) val), columnName);
            break;
    }
}

//Close data
void close_data(data* dt) {
    free(dt->dataHeader);
    free(dt->bytes);
    free(dt);
}

//Try to insert data. If insertion was successful, returns true.
bool insert_data(data* dt, database* db) {
    bool success = false;
    page* pg = dt->table->firstAvailableForWritePage;
    uint32_t offset = dt->table->header->oneRowSize * pg->header->rowsAmount;
    memcpy(pg->bytes + offset, dt->bytes, dt->pointer);
    pg->header->rowsAmount++;
    pg->header->freeRowOffset += pg->tableHeader->oneRowSize;
    if (!page_has_space_for_write(pg)) {
        pg->header->free = false;
        dt->table->firstAvailableForWritePage = allocate_page(db);
        pg->header->nextPageOrdinal = dt->table->firstAvailableForWritePage->header->pageOrdinal;
        table_page_link(dt->table->firstAvailableForWritePage, dt->table);
        if (!pg->nextPage) {
            pg->nextPage = dt->table->firstAvailableForWritePage;
        }
        if (pg != dt->table->firstPage) {
            if (db) {
                success = write_page_to_file(pg, db->file);
                write_page_to_file(dt->table->firstAvailableForWritePage, db->file);
                close_page(pg);
            }
            close_data(dt);
            return success;
        }
    }
    success = write_page_to_file(pg, db->file);
    close_data(dt);
    return success;
}

// Close database
void close_database(database* db) {
    write_database_header(db->file, db->header);
    free(db->header);
}
