#ifndef LLP_LAB1_UTIL_H
#define LLP_LAB1_UTIL_H

#include <time.h>

// Возвратить текущее время
struct timespec get_current_time();
// Получить разницу 
long long get_time_difference_microseconds(struct timespec start_time, struct timespec end_time);

#endif // LLP_LAB1_UTIL_H
