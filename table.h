#ifndef LLP_LAB1_TABLE_H
#define LLP_LAB1_TABLE_H

typedef enum columnType columnType;
typedef struct columnHeader columnHeader;
typedef struct column column;
typedef struct tableHeader tableHeader;
typedef struct table table;

#include <inttypes.h>
#include "database.h"
#include "data.h"
#include <string.h>
#include <stdlib.h>

#define MAX_NAME_LENGTH 31
#define DEFAULT_STRING_LENGTH 255

enum columnType {
    INTEGER, STRING, BOOLEAN, FLOAT
};

struct columnHeader {
    columnType type;
    uint16_t size;
    char columnName[MAX_NAME_LENGTH];
};

struct column {
    columnHeader* header;
    char* columnName;
};

struct tableHeader {
    uint32_t thisSize;
    uint16_t oneRowSize;
    uint16_t columnAmount;
    uint16_t tableNameLength;
    char tableName[MAX_NAME_LENGTH];
    columnHeader columns[];
};

struct table{
    database* db;
    tableHeader* header;
    page* firstPage;
    page* firstAvailableForWritePage;
};

// Создать таблицу
table* create_new_table(const char* name, uint8_t columnAmount);
// Открыть таблицу
table* open_table(database* db, const char* name);
// Закрыть таблицу
void close_table(table* tb);
// Получить размер столбца для заданного типа данных
uint8_t type_to_column_size(columnType type);
// Инициализировать столбец с фиксированным размером
void initialize_fixed_size_column(table* tb, uint8_t ordinal, const char* name, columnType type);
// Инициализировать столбец с переменным размером
void initialize_var_size_column(table* tb, uint8_t ordinal, const char* name, columnType type, uint16_t size);
// Применить изменения для базы данных
void table_apply(database* db, table* table);
// Получить указатель на заголовок столбца по имени столбца в таблице
columnHeader* get_column_header_by_name(table* tb, const char* name);

#endif //LLP_LAB1_TABLE_H
