#ifndef LLP_LAB1_DATABASE_H
#define LLP_LAB1_DATABASE_H

#define DEFAULT_PAGE_SIZE 4096

typedef struct db_header db_header;
typedef struct database database;
typedef struct pageHeader pageHeader;
typedef struct page page;

#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>
#include <malloc.h>
#include "table.h"



struct pageHeader {
    bool free;
    uint32_t pageOrdinal;
    uint16_t rowsAmount;
    uint32_t freeRowOffset;
    uint32_t nextPageOrdinal;
};

struct page {
    pageHeader* header;
    tableHeader* tableHeader;
    page* nextPage;
    void* bytes;
};

struct db_header {
    uint64_t pageSize;
    uint32_t pagesAmount;
    uint16_t firstFreePageOrdinal;
};

struct database {
    FILE* file;
    db_header* header;
};

bool write_database_header(FILE* file, db_header* header);
page* allocate_page(database* db);
page* read_page(database* db, uint32_t pageOrdinal);
page* get_first_available_for_write_page(database* db, table* table);
bool write_page_to_file(page* pg, FILE* file);
database* initialize_database(FILE* file, bool overwrite);
void close_database(database* db);
void close_page(page* pg);
bool page_has_space_for_write(page* pg);
void table_page_link(page* pg, table* tb);


#endif //LLP_LAB1_DATABASE_H
