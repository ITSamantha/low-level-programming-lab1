#include "util.h"
#define NANOS_IN_SECOND 1000000000

struct timespec get_current_time()
{
    struct timespec times;
    timespec_get(&times, TIME_UTC);
    return times;
}

long long get_time_difference_microseconds(struct timespec start_time, struct timespec end_time) {
    long long sec1 = start_time.tv_sec;
    long long sec2 = end_time.tv_sec;
    long long ns1 = start_time.tv_nsec;
    long long ns2 = end_time.tv_nsec;

    if (ns2 > ns1) {
        return (sec2 - sec1) * 1000000 + (ns2 - ns1) / 1000;
    } else {
        return (sec2 - sec1 - 1) * 1000000 + (NANOS_IN_SECOND + ns2 - ns1) / 1000;
    }
}
