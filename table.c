#include "table.h"

uint8_t type_to_column_size(columnType type) {
    switch (type) {
        case INTEGER:
            return sizeof(int32_t);
        case BOOLEAN:
            return sizeof(bool);
        case STRING:
            return sizeof(char) * DEFAULT_STRING_LENGTH;
        case FLOAT:
            return sizeof (double);
        default:
            return 0;
    }
}

columnHeader* get_column_header_by_name(table* tb, const char* name) {
    for (int i = 0; i < tb->header->columnAmount; i++) {
        columnHeader* currentColumn = &tb->header->columns[i];
        if (strcmp(name, currentColumn->columnName) == 0) {
            return currentColumn;
        }
    }
    return NULL;
}

table* create_new_table(const char* name, uint8_t columnAmount) {
    table* tb = malloc(sizeof (table));

    tableHeader* header = malloc(sizeof (tableHeader) + columnAmount * sizeof (columnHeader));
    header->columnAmount = columnAmount;
    header->tableNameLength = strlen(name);
    header->oneRowSize = sizeof(dataHeader);
    header->thisSize = sizeof(tableHeader) + columnAmount * sizeof(columnHeader);

    if (header->tableNameLength > MAX_NAME_LENGTH) {
        printf("Wow! Name length exceeded.");
        return NULL;
    }

    for (int i = 0; i < header->tableNameLength; i++) {
        header->tableName[i] = name[i];
    }
    
    header->tableName[header->tableNameLength] = '\0';

    tb->header = header;

    return tb;
}

void table_apply(database* db, table* table) {
    page* pg = allocate_page(db);
    table_page_link(pg, table);
    table->firstPage = pg;
    table->firstAvailableForWritePage = pg;
    table->db = db;
    if (db) {
        write_page_to_file(pg, db->file);
    }
}


void initialize_fixed_size_column(table* tb, uint8_t ordinal, const char* name, columnType type) {
    columnHeader header = {type, type_to_column_size(type)};
    tb->header->columns[ordinal] = header;
    tb->header->oneRowSize += header.size;
    int len = -1;
    while (name[++len] != '\0') tb->header->columns[ordinal].columnName[len] = name[len];
    tb->header->columns[ordinal].columnName[len] = '\0';
}

void initialize_var_size_column(table* tb, uint8_t ordinal, const char* name, columnType type, uint16_t size) {
    columnHeader header = {type, size};
    tb->header->columns[ordinal] = header;
    tb->header->oneRowSize += header.size;
    int len = -1;
    while (name[++len] != '\0') tb->header->columns[ordinal].columnName[len] = name[len];
    tb->header->columns[ordinal].columnName[len] = '\0';
}

table* open_table(database* db, const char* name) {
    page* currentPage = read_page(db, 0);
    int pageCount = 0;
    while (strcmp(currentPage->tableHeader->tableName, name) != 0 && pageCount < db->header->pagesAmount) {
        close_page(currentPage);
        currentPage = read_page(db, ++pageCount);
    }
    table* tb = malloc(sizeof(table));
    tb->header = currentPage->tableHeader;
    tb->firstPage = currentPage;
    tb->db = db;
    tb->firstAvailableForWritePage = get_first_available_for_write_page(db, tb);
    return tb;
}

void close_table(table* tb) {
    close_page(tb->firstAvailableForWritePage);
    if (tb->firstAvailableForWritePage != tb->firstPage) {
        close_page(tb->firstPage);
    }
    free(tb->header);
    free(tb);
}
