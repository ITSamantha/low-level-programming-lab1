#include "data_iterator.h"


data_iterator* initialize_iterator(database* db, table* tb) {
    data_iterator* iter = malloc(sizeof (data_iterator));
    iter->tb = tb;
    iter->db = db;
    iter->currentPage = tb->firstPage;
    iter->pointer = 0;
    iter->rowsReadOnPage = 0;
    return iter;
}

int32_t get_offset_to_column_data(data_iterator* iter, const char* columnName, columnType colType) {
    int32_t offsetToColumnData = sizeof(dataHeader);
    bool match = false;
    for (int i = 0; i < iter->tb->header->columnAmount; i++) {
        columnHeader currentColumn = iter->tb->header->columns[i];
        if (currentColumn.type == colType && strcmp(columnName, currentColumn.columnName) == 0) {
            match = true;
            break;
        }
        offsetToColumnData += currentColumn.size;
    }
    if (!match) return NONE_OFFSET_TO_COLUMN_DATA;
    return offsetToColumnData;
}

bool has_next(data_iterator* iter) {
    return iter->pointer + iter->tb->header->oneRowSize <= DEFAULT_PAGE_SIZE - sizeof(tableHeader) - sizeof(pageHeader)
    && iter->rowsReadOnPage < iter->currentPage->header->rowsAmount;
}

bool seek_next(data_iterator* iter) {
    dataHeader dHeader = {false};
    uint32_t nextPageNumber = iter->currentPage->header->nextPageOrdinal;
    while (has_next(iter) || nextPageNumber != 0) {
        do {
            if (iter->rowsReadOnPage > 0) {
                iter->pointer += iter->tb->header->oneRowSize;
            }
            dHeader = get_data_header(iter);
            iter->rowsReadOnPage += 1;
        } while (has_next(iter) && !dHeader.is_valid);

        if (dHeader.is_valid) {
            return true;
        }

        if (iter->currentPage != iter->tb->firstPage && iter->currentPage != iter->tb->firstAvailableForWritePage) {
            close_page(iter->currentPage);
        }
        if (nextPageNumber == 0) {
            break;
        }
        iter->currentPage = read_page(iter->db, nextPageNumber);
        nextPageNumber = iter->currentPage->header->nextPageOrdinal;
        iter->rowsReadOnPage = 0;
        iter->pointer = 0;
    }
    return false;
}

bool seek_next_where(data_iterator* iter, const char* colName, columnType colType, const void* val) {
    dataHeader dHeader = {false};

    int32_t colInt;
    char* colChar;
    bool colBool;
    double colFloat;
    uint32_t nextPageNumber = iter->currentPage->header->nextPageOrdinal;
    while (has_next(iter) || nextPageNumber != 0) {
        do {
            if (iter->rowsReadOnPage > 0) {
                iter->pointer += iter->tb->header->oneRowSize;
            }
            dHeader = get_data_header(iter);
            iter->rowsReadOnPage += 1;
        } while (has_next(iter) && !dHeader.is_valid);

        if (dHeader.is_valid) {
            switch (colType) {
                case INTEGER:
                    get_integer(iter, colName, &colInt);
                    if (colInt == *((int32_t *) val)) {
                        return true;
                    }
                    continue;
                case STRING:
                    colChar = malloc(sizeof(char) * DEFAULT_STRING_LENGTH);
                    get_string(iter, colName, &colChar);
                    if (strcmp(colChar, *((char**) val)) == 0) {
                        free(colChar);
                        return true;
                    }
                    free(colChar);
                    continue;
                case BOOLEAN:
                    get_bool(iter, colName, &colBool);
                    if (colBool == *((bool *) val)) {
                        return true;
                    }
                    continue;
                case FLOAT:
                    get_float(iter, colName, &colFloat);
                    if (colFloat == *((double *) val)) {
                        return true;
                    }
                    continue;
                default:
                    break;
            }
        }

        if (iter->currentPage != iter->tb->firstPage && iter->currentPage != iter->tb->firstAvailableForWritePage) {
            close_page(iter->currentPage);
        }
        if (nextPageNumber == 0) {
           break;
        }
        iter->currentPage = read_page(iter->db, nextPageNumber);
        nextPageNumber = iter->currentPage->header->nextPageOrdinal;
        iter->rowsReadOnPage = 0;
        iter->pointer = 0;
    }
    return false;
}

dataHeader get_data_header(data_iterator* iter) {
    return *((dataHeader*)((char*) iter->currentPage->bytes + iter->pointer));
}

bool get_custom(data_iterator* iter, const char* columnName, void* dest) {
    for (int i = 0; i < iter->tb->header->columnAmount; i++) {
        columnHeader currentColumn = iter->tb->header->columns[i];
        if (strcmp(columnName, currentColumn.columnName) == 0) {
            switch (currentColumn.type) {
                case INTEGER:
                    return get_integer(iter, columnName, dest);
                case STRING:
                    return get_string(iter, columnName, (char **) &dest);
                case BOOLEAN:
                    return get_bool(iter, columnName, dest);
                case FLOAT:
                    return get_float(iter, columnName, dest);
                default:
                    return false;
            }
        }
    }
    return false;
}

bool get_integer(data_iterator* iter, const char* columnName, int32_t* dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, INTEGER);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    *dest = *((int32_t*)((char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData));
    return true;
}

bool get_string(data_iterator* iter, const char* columnName, char** dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, STRING);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    strcpy(*dest, (char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData);
    return true;
}

bool get_bool(data_iterator* iter, const char* columnName, bool* dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, BOOLEAN);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    *dest = *((bool*)((char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData));
    return true;
}

bool get_float(data_iterator* iter, const char* columnName, double* dest) {
    int32_t offsetToColumnData = get_offset_to_column_data(iter, columnName, FLOAT);
    if (offsetToColumnData == NONE_OFFSET_TO_COLUMN_DATA) return false;
    *dest = *((double *)((char*) iter->currentPage->bytes + iter->pointer + offsetToColumnData));
    return true;
}

uint16_t delete_where(database* db, table* tb, const char* colName, columnType colType, const void* val) {
    data_iterator* iter = initialize_iterator(db, tb);
    uint16_t removedObjects = 0;

    while (seek_next_where(iter, colName, colType, val)) {
        page* pg = iter->currentPage;
        void* pageBytes = pg->bytes;
        dataHeader header = get_data_header(iter);
        header.is_valid = false;
        memcpy(pageBytes + iter->pointer, &header, sizeof(dataHeader));
        write_page_to_file(pg, db->file);
        removedObjects++;
    }
    free(iter);
    return removedObjects;
}

uint16_t update_where(database* db, table* tb,
                     const char* whereColName, columnType whereColType, const void* whereVal,
                     const char* updateColName, columnType updateColType, const void* updateVal) {
    data_iterator* iter = initialize_iterator(db, tb);
    uint16_t updatedObjects = 0;
    while (seek_next_where(iter, whereColName, whereColType, whereVal)) {
        page* pg = iter->currentPage;
        void* pageBytes = pg->bytes;
        int32_t columnDataOffset = get_offset_to_column_data(iter, updateColName, updateColType);
        columnHeader* c_header = get_column_header_by_name(iter->tb, updateColName);
        memcpy(pageBytes + iter->pointer + columnDataOffset, updateVal, c_header->size);
        write_page_to_file(pg, db->file);
        updatedObjects++;
    }
    free(iter);
    return updatedObjects;
}


void print_join_table(database* db, table* tb1, table* tb2, const char* onColumnT1, const char* onColumnT2, columnType type) {

    data_iterator* iter1 = initialize_iterator(db,tb1);
    data_iterator* iter2 = initialize_iterator(db, tb2);
    while (seek_next(iter1)) {
        columnHeader* onColumnT1Header = get_column_header_by_name(tb1, onColumnT1);
        void* value = malloc(onColumnT1Header->size);
        get_custom(iter1, onColumnT1, value);
        while (seek_next_where(iter2, onColumnT2, type, value)) {
            for (int i = 0; i < iter1->tb->header->columnAmount; i++) {
                columnHeader cheader = iter1->tb->header->columns[i];
                void* curColumnValue = malloc(cheader.size);
                get_custom(iter1, cheader.columnName, curColumnValue);
                switch (cheader.type) {
                    case INTEGER:
                        printf("\t %d \t", *((int32_t*) curColumnValue));
                        break;
                    case STRING:
                        printf("\t%s\t", (char*) curColumnValue);
                        break;
                    case BOOLEAN:
                        printf("\t%d\t", *((bool*) curColumnValue));
                        break;
                    case FLOAT:
                        printf("\t%f\t", *((float*) curColumnValue));
                        break;
                }
            }

            for (int i = 0; i < iter2->tb->header->columnAmount; i++) {
                columnHeader cheader = iter2->tb->header->columns[i];
                void* curColumnValue = malloc(cheader.size);
                get_custom(iter2, cheader.columnName, curColumnValue);
                switch (cheader.type) {
                    case INTEGER:
                        printf("\t%d\t", *((int32_t*) curColumnValue));
                        break;
                    case STRING:
                        printf("\t%s\t", (char*) curColumnValue);
                        break;
                    case BOOLEAN:
                        printf("\t%d\t", *((bool*) curColumnValue));
                        break;
                    case FLOAT:
                        printf("\t%f\t", *((float*) curColumnValue));
                        break;
                }
            }

            printf("\n");
        }
        iter2 = initialize_iterator(db, tb2);
    }
    free(iter1);
    free(iter2);

}
