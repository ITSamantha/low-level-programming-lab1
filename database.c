#include "database.h"


page* allocate_page(database* db) {
    page* pg = malloc(sizeof (page));

    pageHeader* header = malloc(sizeof(pageHeader));
    header->free = true;
    header->pageOrdinal = db ? db->header->pagesAmount++ : 0;
    header->rowsAmount = 0;
    header->freeRowOffset = 0;
    header->nextPageOrdinal = 0;

    pg->header = header;
    pg->tableHeader = NULL;
    pg->nextPage = NULL;
    pg->bytes = NULL; 

    if (db) {
        write_database_header(db->file, db->header);
    }

    return pg;
}

bool read_database_header(FILE* file, db_header* header) {
    fseek(file, 0, SEEK_SET);
    size_t read = fread(header, sizeof(db_header), 1, file);
    if (read != 1) {
        return false;
    }
    return true;
}

bool write_database_header(FILE* file, db_header* header) {
    fseek(file, 0, SEEK_SET);
    size_t written = fwrite(header, sizeof(db_header), 1, file);
    if (written != 1) {
        return false;
    }
    return true;
}


bool write_page_to_file(page* pg, FILE* file) {
    fseek(file, sizeof(db_header) + pg->header->pageOrdinal * DEFAULT_PAGE_SIZE, SEEK_SET);
    size_t pageHeaderSize = sizeof (pageHeader);
    size_t tableHeaderSize = pg->tableHeader != NULL ? pg->tableHeader->thisSize : 0;
    bool success = true;
    success = success & fwrite(pg->header, pageHeaderSize, 1, file);
    if (pg->tableHeader) {
        success &= fwrite(pg->tableHeader, tableHeaderSize, 1, file);
    }
    if (pg->bytes) {
        success &= fwrite(pg->bytes, DEFAULT_PAGE_SIZE - tableHeaderSize - pageHeaderSize, 1, file);
    }
    return success;
}



database* read_database(FILE* file) {
    database* db = malloc(sizeof(database));
    db_header* header = malloc(sizeof(db_header));
    if (!read_database_header(file, header)) {
        printf("Error reading db header");
        exit(EXIT_FAILURE);
    }
    db->header = header;
    db->file = file;
    return db;
}

database* overwrite_database(FILE* file) {
    database* db = malloc(sizeof(database));
    db_header* header = malloc(sizeof(db_header));

    header->pageSize = DEFAULT_PAGE_SIZE;
    header->pagesAmount = 0;
    header->firstFreePageOrdinal = 0;

    db->header = header;
    db->file = file;

    write_database_header(file, header);

    return db;
}


database* initialize_database(FILE* file, bool overwrite) {
    if (overwrite) {
        return overwrite_database(file);
    }
    return read_database(file);
}

bool page_has_space_for_write(page* pg) {
    tableHeader* tbHeader = pg->tableHeader;
    return tbHeader->thisSize + sizeof(pageHeader) + pg->header->rowsAmount * tbHeader->oneRowSize
    <= DEFAULT_PAGE_SIZE - tbHeader->oneRowSize;
}

void table_page_link(page* pg, table* tb) {
    pg->tableHeader = tb->header;
    pg->bytes = malloc(DEFAULT_PAGE_SIZE - sizeof(pageHeader) - pg->tableHeader->thisSize);
}

page* get_first_available_for_write_page(database* db, table* table) {
    page* pg = table->firstPage;
    while (!pg->header->free) {
        uint32_t nextPageOrdinal = pg->header->nextPageOrdinal;
        if (pg->header->nextPageOrdinal == 0) {
            page* newPage = allocate_page(db);
            pg->header->nextPageOrdinal = newPage->header->pageOrdinal;
            table_page_link(newPage, table);
            write_page_to_file(pg, db->file);
            write_page_to_file(newPage, db->file);
            return newPage;
        }
        if (pg != table->firstPage) {
            close_page(pg);
        }
        pg = read_page(db, nextPageOrdinal);
    }
    return pg;
}


page* read_page(database* db, uint32_t pageOrdinal) {
    if (pageOrdinal >= db->header->pagesAmount) {
        printf("Bad page ordinal");
        return NULL;
    }
    page* pg = malloc(sizeof (page));
    FILE* file = db->file;
    fseek(file, sizeof(db_header) + pageOrdinal * DEFAULT_PAGE_SIZE, SEEK_SET);

    pageHeader* pgHeader = malloc(sizeof(pageHeader));
    size_t readPgHeader = fread(pgHeader, sizeof(pageHeader), 1, file);
    if (readPgHeader < 1) {
        printf("Error reading page header.");
    }
    uint32_t tableHeaderSize;
    size_t read = fread(&tableHeaderSize, sizeof(uint32_t), 1, file);
    tableHeader* tbHeader = malloc(tableHeaderSize);
    fseek(file, -sizeof(uint32_t), SEEK_CUR);
    size_t readTbHeader = fread(tbHeader, tableHeaderSize, 1, file);
    if (readTbHeader < 1) {
        printf("Error reading table header.");
    }
    pg->bytes = malloc(DEFAULT_PAGE_SIZE - tableHeaderSize - sizeof(pageHeader));
    size_t readBytes = fread(pg->bytes, DEFAULT_PAGE_SIZE - tableHeaderSize - sizeof(pageHeader), 1, file);
    if (readBytes < 1) {
        printf("Error reading bytes");
    }
    pg->header = pgHeader;
    pg->tableHeader = tbHeader;
    pg->nextPage = NULL;

    return pg;
}

void close_page(page* pg) {
    free(pg->header);
    free(pg->bytes);
    free(pg);
}



